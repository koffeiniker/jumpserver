[Wiki](https://bitbucket.com/koffeiniker/jumpserver/wiki/Home) | [Privacy Statement](https://www.atlassian.com/legal/privacy-policy) | [Legal notice](https://road-coder.de/legal/impressum-en/)

# JumpServer

[TOC]

# What is this about?

For purpose of system- and application-administration, often there is need
for a so-called jumpserver. Usually these are under-equipped and badly prepared.
This nano-project holds the Vagrant-setup for a reasonably nice jumpserver based on Centos.

Remember, this is just the README, you might want to see the [wiki](http:/koffeiniker/jumpserver/wiki)!

# Who is this for?

You are a seasoned admin looking to save some time. You are working with
server applications sitting in isolated networks. You need to access your
applications via a single SSH interface. You might deploy your servers and
applications via Ansible and you maintain scripts or programs to do some of
your operative work.

This is for you!

You should know or learn about: Ansible, Vagrant, VMware, ssh. This is to get the
thing going and maintain security.

# What's in the box?

Console-food:

- Midnight Commander ... ancient ... I know ... I am old!
- screen

System:

- epel is configured

Network stuff:

- wget, telnet, nmap, ncat, ifconfig

Documentation:

- pandoc
- LaTeX (for pandoc)

For deployments:

- git
- ansible

For development (small stuff, stay on the ground):

- Python 3.6
- TCL
- Go

For publishing:

- Hugo



# Security

Please care for your security. This means a couple of things to be cared for.



## Your SSH key

The account that builds the VM using Vagrant should have a well protected pair
of SSH-keys. They must be rsa-encrypted. The public rsa-key of this account will
automatically be added to the authorized_key file of the vagrant and orgadmin
users in the VM.

** It is highly recommended to use passwords on SSH-keys! **



## The static SSH key

Before you can build and deploy the jumpserver, you need to run

```
./gen_static_keys
```

in your project-directory.

This will generate two files in the ./files subdirectory.
One of these (files/sshkeys.pub) will be
registered as authorized_key in the orgadmin user. The other one is the
correspondig private key. You may copy and use this.

In order to login:

```
ssh -i files/sshkeys orgadmin@your-server-address.somewhere
````

The files will usually not be changed by deployments and their will not be
handled by git. This is a security precaution. If you want to keep them,
copy them into a safe place, i.e. where you will use them or e.g. into a
keepass-database.

**Again! When generating the static keys ... always use  a password with them!**



# The annoying legal stuff

Help yourself, I do not claim any rights on this little playbook.

Disclaimer: I am not responsible for anything that happens to your or your
equipment or data when using this JumpServer or any derived work. And for sure,
I make no claim about security of this setup. If you are interested in this,
you will likely be a seasoned admin: base your own work on this. Do not rely
on _anything_!

I tried to save everybody some time by writing this Playbook, this explicitely does
not void your responsibility to check and check again the security and fitness for your
purpose.

Whatever you do with this, is solely your responsibility.

GOT it? GIT it!
